#include "dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
}

Dialog::~Dialog()
{

}


void Dialog::mousePressEvent ( QMouseEvent * event ) {
    // output the x,y coordinates
    qDebug() << "clicked at: " << event->x() << "," << event->y();
}


void Dialog::paintEvent(QPaintEvent *event) {
    // the QPainter is the 'canvas' to which we will draw
    // the QPen is the pen that will be used to draw to the 'canvas'
    QPainter p(this);
    QPen myPen;

    myPen.setWidth(10);
    myPen.setColor(QColor(0xff0000));
    p.setPen(myPen);

    p.drawEllipse(width()/2,height()/2,20,20);
}
